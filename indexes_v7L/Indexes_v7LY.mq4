//+------------------------------------------------------------------+
//|                                                Indexes_v7L-Y.mq4 |
//|                                          Copyright  2008, Xupypr |
//+------------------------------------------------------------------+
#property copyright "Copyright ｩ 2008, Xupypr"

#property indicator_separate_window
#property indicator_buffers 8
#property indicator_level1 100
#property indicator_level2 -100
#property indicator_levelcolor LightSlateGray
#property indicator_color1 Orange
#property indicator_color2 Red
#property indicator_color3 Lime
#property indicator_color4 Turquoise
#property indicator_color5 White
#property indicator_color6 RoyalBlue
#property indicator_color7 Yellow
#property indicator_color8 LightPink
#property indicator_width1 1
#property indicator_width2 1
#property indicator_width3 1
#property indicator_width4 1
#property indicator_width5 1
#property indicator_width6 1
#property indicator_width7 1
#property indicator_width8 1
//---- input parameters
extern string start_time="2017.1.3 00:00"; // ・ⅳ (・淲・糂・竟蒟・・粹・100%)
extern bool   use_start_time=false;          // 頌・・鉋籵 ・ⅳ, 竟璞・碣瑣・燾 鈞・靑・ 淲蒟・
extern bool   auto_detect_pair=false;        // ⅰ蒟・ 珞・・籵・・ ・ ・ⅳ髜赳 ⅳ粢糒・竟蒟・・
extern bool   diff_indicators=false;         // 頸瑣・鈿ⅲ 竟蒟・魵/竟蒻・・・跖・ "珞ⅰ蒟・湜" ・
extern bool   reverse_index=false;           // "・粽籵" 竟蒟・ 籵・, ・・・・ 糘ⅱ鵫
extern int    applied_price=0;               // 頌・・銛褌・ 浯: 0-CLOSE; 1-OPEN; 2-HIGH; 3-LOW; 4-MEDIAN; 5-TYPICAL; 6-WEIGHTED;
extern int    ma_period=1;                   // ・鮏 蓖褊・ 蓁 糺褊・ 鸙・胛 裝淲胛
extern int    ma_method=3;                   // ・・蓖褊・: 0-MODE_SMA; 1-MODE_EMA; 2-MODE_SMMA; 3-MODE_LWMA
extern int    select_indicator=0;            // 0-竟蒟・・籵・・砒・竟蒻・・ 1-CCI; 2-RSI; 3-Momentum; 4-MACD; 5-Stochastic;
extern int    period_indicator=14;           // ・鮏 蓖褊・ 蓁 糺褊・ 竟蒻・
extern int    fast_EMA=12;                   // 蓁 竟蒻・ MACD
extern int    slow_EMA=26;                   // 蓁 竟蒻・ MACD
extern int    k_period=5;                    // 蓁 竟蒻・ Stochastic
extern int    slowing=3;                     // 蓁 竟蒻・ Stochastic
extern bool   show_USD=true;
extern bool   show_EUR=true;
extern bool   show_GBP=true;
extern bool   show_JPY=true;
extern bool   show_CHF=true;
extern bool   show_AUD=true;
extern bool   show_CAD=true;
extern bool   show_NZD=true;
//---- buffers
double iUSDBuffer[];
double iEURBuffer[];
double iGBPBuffer[];
double iJPYBuffer[];
double iCHFBuffer[];
double iAUDBuffer[];
double iCADBuffer[];
double iNZDBuffer[];
double xUSDBuffer[];
double xEURBuffer[];
double xGBPBuffer[];
double xJPYBuffer[];
double xCHFBuffer[];
double xAUDBuffer[];
double xCADBuffer[];
double xNZDBuffer[];
//---- indicator buffers
double USDBuffer[];
double EURBuffer[];
double GBPBuffer[];
double JPYBuffer[];
double CHFBuffer[];
double AUDBuffer[];
double CADBuffer[];
double NZDBuffer[];
//---- indicator parameters
int      i,BarsCS,LB;
bool     Show_CUR[8],Stop=false;
double   SPrice[28];
string   CS1,CS2;
string   Currency[8]={"USD","EUR","GBP","JPY","CHF","AUD","CAD","NZD"};
string   Symbols[28]={"AUDCAD","AUDCHF","AUDJPY","AUDNZD","AUDUSD","CADCHF","CADJPY","CHFJPY","EURAUD","EURCAD","EURCHF","EURGBP","EURJPY","EURNZD","EURUSD","GBPAUD","GBPCAD","GBPCHF","GBPJPY","GBPUSD","NZDCAD","NZDCHF","GBPNZD","NZDJPY","NZDUSD","USDCAD","USDCHF","USDJPY"};
datetime Period_End,SPoint;

//+------------------------------------------------------------------+
//| Custom indicator initialization function                         |
//+------------------------------------------------------------------+
int init()
{
 int     SBar,STime[29];
 bool    Show=false;
 string  Name;
 
 Comment("");
 if (Period()==PERIOD_MN1)
 {
  Comment("ﾏ褞韶・淲 ・趺・磊 碚・ W1"); 
  Stop=true;
  return(-1);
 }
 if (auto_detect_pair)
 {
  ArrayInitialize(Show_CUR,false);
  CS1=StringSubstr(Symbol(),0,3);
  CS2=StringSubstr(Symbol(),3,3);
  for (i=0;i<8;i++)
  {
   if (Currency[i]==CS1 || Currency[i]==CS2)
   {
    Show_CUR[i]=true;
    Show=true;
   }
  }
  if (Show==false)
  {
   ArrayInitialize(Show_CUR,true);
   Comment("ﾄ瑙濵・ 竟・炅・淲 ⅳ粢・湜 鮏竟 竟蒟・");
  } 
 }
 else
 {
  Show_CUR[0]=show_USD;
  Show_CUR[1]=show_EUR;
  Show_CUR[2]=show_GBP;
  Show_CUR[3]=show_JPY;
  Show_CUR[4]=show_CHF;
  Show_CUR[5]=show_AUD;
  Show_CUR[6]=show_CAD;
  Show_CUR[7]=show_NZD;
 }
 for (i=0;i<28;i++)
 {
  SBar=iBars(Symbols[i],0);
  if (SBar==0)
  {
   Comment("ﾎ糒褪 頌 蓁 "+Symbols[i]);
   Stop=true;
   return(-1);
  }
  STime[i]=iTime(Symbols[i],0,SBar-1);
 }
 SBar=iBars(NULL,0);
 STime[28]=iTime(NULL,0,SBar-1);
 SPoint=STime[ArrayMaximum(STime)];
 BarsCS=iBarShift(NULL,0,SPoint);
 if (use_start_time)
 {
  Period_End=StrToTime(start_time);
  if (Period_End<SPoint)
  {
   Period_End=SPoint+60*Period()*(period_indicator+1);
   Comment("ﾂ鎤浯 蓿・ ・ⅳ - "+TimeToStr(Period_End));
  } 
 } 
 else
 {
  i=0;
  while ((iTime(NULL,0,i+1)+172800)>iTime(NULL,0,i)) i++;
  Period_End=iTime(NULL,0,i+1);
 }
 if (applied_price<0) applied_price=0;
 if (applied_price>6) applied_price=6;
 for (i=0;i<28;i++) SPrice[i]=Price(i,Period_End);
 Name="Indexes( ";
 for (i=0;i<8;i++) if (Show_CUR[i])
 {
  switch (i)
  {
   case 0: {SetIndexBuffer(0,USDBuffer); ArraySetAsSeries(iUSDBuffer,true); ArraySetAsSeries(xUSDBuffer,true);} break;
   case 1: {SetIndexBuffer(1,EURBuffer); ArraySetAsSeries(iEURBuffer,true); ArraySetAsSeries(xEURBuffer,true);} break;
   case 2: {SetIndexBuffer(2,GBPBuffer); ArraySetAsSeries(iGBPBuffer,true); ArraySetAsSeries(xGBPBuffer,true);} break;
   case 3: {SetIndexBuffer(3,JPYBuffer); ArraySetAsSeries(iJPYBuffer,true); ArraySetAsSeries(xJPYBuffer,true);} break;
   case 4: {SetIndexBuffer(4,CHFBuffer); ArraySetAsSeries(iCHFBuffer,true); ArraySetAsSeries(xCHFBuffer,true);} break;
   case 5: {SetIndexBuffer(5,AUDBuffer); ArraySetAsSeries(iAUDBuffer,true); ArraySetAsSeries(xAUDBuffer,true);} break;
   case 6: {SetIndexBuffer(6,CADBuffer); ArraySetAsSeries(iCADBuffer,true); ArraySetAsSeries(xCADBuffer,true);} break;
   case 7: {SetIndexBuffer(7,NZDBuffer); ArraySetAsSeries(iNZDBuffer,true); ArraySetAsSeries(xNZDBuffer,true);}
  }
  SetIndexStyle(i,DRAW_LINE);
  if (reverse_index && Currency[i]==CS2) SetIndexLabel(i,Currency[i]+" reverse");
  else SetIndexLabel(i,Currency[i]);
  Name=StringConcatenate(Name,Currency[i]," ");
 } 
 Name=Name+")";
 if (ma_period<1) ma_period=1;
 if (ma_method<0) ma_method=0;
 if (ma_method>3) ma_method=3;
 if (select_indicator<0) select_indicator=0;
 if (select_indicator>5) select_indicator=5;
 if (period_indicator<1) period_indicator=1;
 switch (select_indicator)
 {
  case 1: Name=StringConcatenate(Name,"+CCI(",period_indicator,")"); break;
  case 2: Name=StringConcatenate(Name,"+RSI(",period_indicator,")"); break;
  case 3: Name=StringConcatenate(Name,"+Mom.(",period_indicator,")"); break;
  case 4: Name=StringConcatenate(Name,"+MACD(",fast_EMA,",",slow_EMA,")"); break;
  case 5: Name=StringConcatenate(Name,"+St.(",k_period,",",slowing,")");
 }
 Name=StringConcatenate(Name,"+MA(",ma_period,",",ma_method,")");
 if (diff_indicators && auto_detect_pair) Name=StringConcatenate(Name,"+diff");
 IndicatorDigits(2);
 IndicatorShortName(Name);
 return(0);
}
//+----------------------------------------------------------------------------+
//|  Custom indicator deinitialization function                                |
//+----------------------------------------------------------------------------+
int deinit()
{
 ObjectDelete("Period_End");
 return(0);
}
//+------------------------------------------------------------------+
//| Indexes foreign exchange                                         |
//+------------------------------------------------------------------+
int start()
{
 int      b,c,Limit;
 double   CurBuffer,Change,CIndex[8];
 string   Nm,Dm;
 datetime BTime,LT;
 static datetime TimeBar;
 
 if (Stop) return(-1);
 if (TimeBar!=Time[0]) 
 {
  TimeBar=Time[0];
  BarsCS=iBarShift(NULL,0,SPoint);
  if (Show_CUR[0]) {ArrayResize(iUSDBuffer,BarsCS+1); ArrayResize(xUSDBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[1]) {ArrayResize(iEURBuffer,BarsCS+1); ArrayResize(xEURBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[2]) {ArrayResize(iGBPBuffer,BarsCS+1); ArrayResize(xGBPBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[3]) {ArrayResize(iJPYBuffer,BarsCS+1); ArrayResize(xJPYBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[4]) {ArrayResize(iCHFBuffer,BarsCS+1); ArrayResize(xCHFBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[5]) {ArrayResize(iAUDBuffer,BarsCS+1); ArrayResize(xAUDBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[6]) {ArrayResize(iCADBuffer,BarsCS+1); ArrayResize(xCADBuffer,BarsCS-period_indicator+1);}
  if (Show_CUR[7]) {ArrayResize(iNZDBuffer,BarsCS+1); ArrayResize(xNZDBuffer,BarsCS-period_indicator+1);}
  for (i=BarsCS;i>0;i--)
  {
   if (Show_CUR[0]) iUSDBuffer[i]=iUSDBuffer[i-1];
   if (Show_CUR[1]) iEURBuffer[i]=iEURBuffer[i-1];
   if (Show_CUR[2]) iGBPBuffer[i]=iGBPBuffer[i-1];
   if (Show_CUR[3]) iJPYBuffer[i]=iJPYBuffer[i-1];
   if (Show_CUR[4]) iCHFBuffer[i]=iCHFBuffer[i-1];
   if (Show_CUR[5]) iAUDBuffer[i]=iAUDBuffer[i-1];
   if (Show_CUR[6]) iCADBuffer[i]=iCADBuffer[i-1];
   if (Show_CUR[7]) iNZDBuffer[i]=iNZDBuffer[i-1];
  }
  for (i=BarsCS-period_indicator;i>0;i--)
  {
   if (Show_CUR[0]) xUSDBuffer[i]=xUSDBuffer[i-1];
   if (Show_CUR[1]) xEURBuffer[i]=xEURBuffer[i-1];
   if (Show_CUR[2]) xGBPBuffer[i]=xGBPBuffer[i-1];
   if (Show_CUR[3]) xJPYBuffer[i]=xJPYBuffer[i-1];
   if (Show_CUR[4]) xCHFBuffer[i]=xCHFBuffer[i-1];
   if (Show_CUR[5]) xAUDBuffer[i]=xAUDBuffer[i-1];
   if (Show_CUR[6]) xCADBuffer[i]=xCADBuffer[i-1];
   if (Show_CUR[7]) xNZDBuffer[i]=xNZDBuffer[i-1];
  }
 }
 if (ObjectFind("Period_End")==-1)
 {
  ObjectCreate("Period_End",OBJ_VLINE,0,Period_End,0);
  ObjectSetText("Period_End","ﾒⅸ・ ⅳ");
  ObjectSet("Period_End",OBJPROP_COLOR,OrangeRed);
  ObjectSet("Period_End",OBJPROP_WIDTH,2);
 } 
 if (Period_End!=ObjectGet("Period_End",OBJPROP_TIME1))
 {
  Period_End=ObjectGet("Period_End",OBJPROP_TIME1);
  for (i=0;i<28;i++)
  {
   SPrice[i]=Price(i,Period_End);
   LB=BarsCS;
  }
 } 
 Limit=BarsCS;
 int counted_bars=IndicatorCounted();
 if (counted_bars>0) Limit=Bars-counted_bars-1;
 if (Limit<LB) Limit=LB;
 for (b=Limit;b>=0;b--)
 {
  ArrayInitialize(CIndex,1.0);
  BTime=iTime(NULL,0,b);
  for (i=0;i<28;i++)
  {
   Change=Price(i,BTime)/SPrice[i];
   Nm=StringSubstr(Symbols[i],0,3);
   Dm=StringSubstr(Symbols[i],3,3);
   for (c=0;c<8;c++) if (Show_CUR[c]) 
   {
    if (Nm==Currency[c]) CIndex[c]*=Change;
    if (Dm==Currency[c]) CIndex[c]/=Change;
   }
  }
  for (c=0;c<8;c++) if (Show_CUR[c])
  {
   CIndex[c]=100*MathPow(CIndex[c],0.125);
   if (reverse_index && Currency[c]==CS2) CIndex[c]=200-CIndex[c];
  }
  if (Show_CUR[0]) iUSDBuffer[b]=CIndex[0];
  if (Show_CUR[1]) iEURBuffer[b]=CIndex[1];
  if (Show_CUR[2]) iGBPBuffer[b]=CIndex[2];
  if (Show_CUR[3]) iJPYBuffer[b]=CIndex[3];
  if (Show_CUR[4]) iCHFBuffer[b]=CIndex[4];
  if (Show_CUR[5]) iAUDBuffer[b]=CIndex[5];
  if (Show_CUR[6]) iCADBuffer[b]=CIndex[6];
  if (Show_CUR[7]) iNZDBuffer[b]=CIndex[7];
 }
 if (Limit>(BarsCS-period_indicator)) Limit=BarsCS-period_indicator;
 for (b=Limit;b>=0;b--)
 {
  switch (select_indicator)
  {
   case 0:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=iUSDBuffer[b];
    if (Show_CUR[1]) xEURBuffer[b]=iEURBuffer[b];
    if (Show_CUR[2]) xGBPBuffer[b]=iGBPBuffer[b];
    if (Show_CUR[3]) xJPYBuffer[b]=iJPYBuffer[b];
    if (Show_CUR[4]) xCHFBuffer[b]=iCHFBuffer[b];
    if (Show_CUR[5]) xAUDBuffer[b]=iAUDBuffer[b];
    if (Show_CUR[6]) xCADBuffer[b]=iCADBuffer[b];
    if (Show_CUR[7]) xNZDBuffer[b]=iNZDBuffer[b];
   } break;
   case 1:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=iCCIOnArray(iUSDBuffer,0,period_indicator,b);
    if (Show_CUR[1]) xEURBuffer[b]=iCCIOnArray(iEURBuffer,0,period_indicator,b);
    if (Show_CUR[2]) xGBPBuffer[b]=iCCIOnArray(iGBPBuffer,0,period_indicator,b);
    if (Show_CUR[3]) xJPYBuffer[b]=iCCIOnArray(iJPYBuffer,0,period_indicator,b);
    if (Show_CUR[4]) xCHFBuffer[b]=iCCIOnArray(iCHFBuffer,0,period_indicator,b);
    if (Show_CUR[5]) xAUDBuffer[b]=iCCIOnArray(iAUDBuffer,0,period_indicator,b);
    if (Show_CUR[6]) xCADBuffer[b]=iCCIOnArray(iCADBuffer,0,period_indicator,b);
    if (Show_CUR[7]) xNZDBuffer[b]=iCCIOnArray(iNZDBuffer,0,period_indicator,b);
   } break;
   case 2:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=iRSIOnArray(iUSDBuffer,0,period_indicator,b);
    if (Show_CUR[1]) xEURBuffer[b]=iRSIOnArray(iEURBuffer,0,period_indicator,b);
    if (Show_CUR[2]) xGBPBuffer[b]=iRSIOnArray(iGBPBuffer,0,period_indicator,b);
    if (Show_CUR[3]) xJPYBuffer[b]=iRSIOnArray(iJPYBuffer,0,period_indicator,b);
    if (Show_CUR[4]) xCHFBuffer[b]=iRSIOnArray(iCHFBuffer,0,period_indicator,b);
    if (Show_CUR[5]) xAUDBuffer[b]=iRSIOnArray(iAUDBuffer,0,period_indicator,b);
    if (Show_CUR[6]) xCADBuffer[b]=iRSIOnArray(iCADBuffer,0,period_indicator,b);
    if (Show_CUR[7]) xNZDBuffer[b]=iRSIOnArray(iNZDBuffer,0,period_indicator,b);
   } break;
   case 3:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=iMomentumOnArray(iUSDBuffer,0,period_indicator,b);
    if (Show_CUR[1]) xEURBuffer[b]=iMomentumOnArray(iEURBuffer,0,period_indicator,b);
    if (Show_CUR[2]) xGBPBuffer[b]=iMomentumOnArray(iGBPBuffer,0,period_indicator,b);
    if (Show_CUR[3]) xJPYBuffer[b]=iMomentumOnArray(iJPYBuffer,0,period_indicator,b);
    if (Show_CUR[4]) xCHFBuffer[b]=iMomentumOnArray(iCHFBuffer,0,period_indicator,b);
    if (Show_CUR[5]) xAUDBuffer[b]=iMomentumOnArray(iAUDBuffer,0,period_indicator,b);
    if (Show_CUR[6]) xCADBuffer[b]=iMomentumOnArray(iCADBuffer,0,period_indicator,b);
    if (Show_CUR[7]) xNZDBuffer[b]=iMomentumOnArray(iNZDBuffer,0,period_indicator,b);
   } break;
   case 4:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=iMAOnArray(iUSDBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iUSDBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[1]) xEURBuffer[b]=iMAOnArray(iEURBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iEURBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[2]) xGBPBuffer[b]=iMAOnArray(iGBPBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iGBPBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[3]) xJPYBuffer[b]=iMAOnArray(iJPYBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iJPYBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[4]) xCHFBuffer[b]=iMAOnArray(iCHFBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iCHFBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[5]) xAUDBuffer[b]=iMAOnArray(iAUDBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iAUDBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[6]) xCADBuffer[b]=iMAOnArray(iCADBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iCADBuffer,0,slow_EMA,0,MODE_EMA,b);
    if (Show_CUR[7]) xNZDBuffer[b]=iMAOnArray(iNZDBuffer,0,fast_EMA,0,MODE_EMA,b)-iMAOnArray(iNZDBuffer,0,slow_EMA,0,MODE_EMA,b);
   } break;
   case 5:
   {
    if (Show_CUR[0]) xUSDBuffer[b]=Stochastic(b,iUSDBuffer);
    if (Show_CUR[1]) xEURBuffer[b]=Stochastic(b,iEURBuffer);
    if (Show_CUR[2]) xGBPBuffer[b]=Stochastic(b,iGBPBuffer);
    if (Show_CUR[3]) xJPYBuffer[b]=Stochastic(b,iJPYBuffer);
    if (Show_CUR[4]) xCHFBuffer[b]=Stochastic(b,iCHFBuffer);
    if (Show_CUR[5]) xAUDBuffer[b]=Stochastic(b,iAUDBuffer);
    if (Show_CUR[6]) xCADBuffer[b]=Stochastic(b,iCADBuffer);
    if (Show_CUR[7]) xNZDBuffer[b]=Stochastic(b,iNZDBuffer);
   }
  }
 }
 if (Limit>(BarsCS-ma_period-period_indicator)) Limit-=ma_period;
 for (b=Limit;b>=0;b--)
 {
  if (Show_CUR[0]) USDBuffer[b]=NormalizeDouble(iMAOnArray(xUSDBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[1]) EURBuffer[b]=NormalizeDouble(iMAOnArray(xEURBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[2]) GBPBuffer[b]=NormalizeDouble(iMAOnArray(xGBPBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[3]) JPYBuffer[b]=NormalizeDouble(iMAOnArray(xJPYBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[4]) CHFBuffer[b]=NormalizeDouble(iMAOnArray(xCHFBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[5]) AUDBuffer[b]=NormalizeDouble(iMAOnArray(xAUDBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[6]) CADBuffer[b]=NormalizeDouble(iMAOnArray(xCADBuffer,0,ma_period,0,ma_method,b),3);
  if (Show_CUR[7]) NZDBuffer[b]=NormalizeDouble(iMAOnArray(xNZDBuffer,0,ma_period,0,ma_method,b),3);
  if (diff_indicators && auto_detect_pair)
  {
   if (CS1==Currency[0]) {CurBuffer=USDBuffer[b]; USDBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[1]) {CurBuffer=EURBuffer[b]; EURBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[2]) {CurBuffer=GBPBuffer[b]; GBPBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[3]) {CurBuffer=JPYBuffer[b]; JPYBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[4]) {CurBuffer=CHFBuffer[b]; CHFBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[5]) {CurBuffer=AUDBuffer[b]; AUDBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[6]) {CurBuffer=CADBuffer[b]; CADBuffer[b]=EMPTY_VALUE;}
   if (CS1==Currency[7]) {CurBuffer=NZDBuffer[b]; NZDBuffer[b]=EMPTY_VALUE;}
   if (CS2==Currency[0]) USDBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[1]) EURBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[2]) GBPBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[3]) JPYBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[4]) CHFBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[5]) AUDBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[6]) CADBuffer[b]=CurBuffer-USDBuffer[b];
   if (CS2==Currency[7]) NZDBuffer[b]=CurBuffer-USDBuffer[b];
  }
 }
 LT=iTime(NULL,0,1);
 for (i=0;i<28;i++) if (iTime(Symbols[i],0,1)<LT) LT=iTime(Symbols[i],0,1);
 LB=iBarShift(NULL,0,LT);
 return(0);
}

double Price(int k, datetime time)
{
 double price;
 int bar=iBarShift(Symbols[k],0,time);
 switch (applied_price)
 {
  case PRICE_CLOSE:    price=iClose(Symbols[k],0,bar); break;
  case PRICE_OPEN:     price=iOpen(Symbols[k],0,bar); break;
  case PRICE_HIGH:     price=iHigh(Symbols[k],0,bar); break;
  case PRICE_LOW:      price=iLow(Symbols[k],0,bar); break;
  case PRICE_MEDIAN:   price=(iHigh(Symbols[k],0,bar)+iLow(Symbols[k],0,bar))/2; break;
  case PRICE_TYPICAL:  price=(iHigh(Symbols[k],0,bar)+iLow(Symbols[k],0,bar)+iClose(Symbols[k],0,bar))/3; break;
  case PRICE_WEIGHTED: price=(iHigh(Symbols[k],0,bar)+iLow(Symbols[k],0,bar)+iClose(Symbols[k],0,bar)+iClose(Symbols[k],0,bar))/4;
 }
 return(price);
}

double Stochastic(int k, double Buffer[])
{
 double Max,Min;
 double Sum1=0;
 double Sum2=0;
 for (int n=0;n<slowing;n++)
 {
  Max=Buffer[ArrayMaximum(Buffer,k_period,k+n)];
  Min=Buffer[ArrayMinimum(Buffer,k_period,k+n)];
  Sum1+=Buffer[k+n]-Min;
  Sum2+=Max-Min;
 }
 if (Sum2==0.0) return(100);
 else return(Sum1/Sum2*100);
}
//+------------------------------------------------------------------+

 